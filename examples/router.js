import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

const router = new Router({
    routes: [
      {
        path: '/',
        component: () => import('./views/layout.vue'),
        children: [
          {
            path: '',
            component: () => import('./views/docs.vue')
          },
          {
            path: '/map',
            component: () => import('./views/index.vue')
          },
          {
            path: 'text',
            component: () => import('./views/text.vue')
          },
          {
            path: 'rectangle',
            component: () => import('./views/rectangle.vue')
          },
          {
            path: 'polygon',
            component: () => import('./views/polygon.vue')
          },
          {
            path: 'circle',
            component: () => import('./views/circle.vue')
          },
          {
            path: 'line',
            component: () => import('./views/line.vue')
          },
          {
            path: 'placement',
            component: () => import('./views/placement.vue')
          },
          {
            path: 'pointer',
            component: () => import('./views/pointer.vue')
          },
          {
            path: 'overview',
            component: () => import('./views/overview.vue')
          },
          {
            path: 'zoom',
            component: () => import('./views/zoom.vue')
          },
          {
            path: 'scale',
            component: () => import('./views/scale.vue')
          },
          {
            path: 'performance',
            component: () => import('./views/performance.vue')
          },
          {
            path: 'image',
            component: () => import('./views/image.vue')
          },
          {
            path: 'html',
            component: () => import('./views/html.vue')
          },
          {
            path: 'icon',
            component: () => import('./views/icon.vue')
          },
          {
            path: 'tooltip',
            component: () => import('./views/tooltip.vue')
          },
          {
            path: 'popup',
            component: () => import('./views/popup.vue')
          },
          {
            path: 'heat',
            component: () => import('./views/heat.vue')
          },
          {
            path: 'echarts',
            component: () => import('./views/echarts.vue')
          },
          {
            path: 'track',
            component: () => import('./views/track.vue')
          },
          {
            path: 'type',
            component: () => import('./views/type.vue')
          },
          {
            path: 'draw',
            component: () => import('./views/draw.vue')
          },
          {
            path: 'scatter',
            component: () => import('./views/scatter.vue')
          },
          {
            path: 'flight',
            component: () => import('./views/flight.vue')
          },
          {
            path: 'measure',
            component: () => import('./views/measure.vue')
          },
          {
            path: 'mask',
            component: () => import('./views/mask.vue')
          },
          {
            path: 'geo',
            component: () => import('./views/geo.vue')
          }
        ]
      }
    ]
    
  }
)

export default router
